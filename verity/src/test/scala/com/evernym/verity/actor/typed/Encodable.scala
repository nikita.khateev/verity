package com.evernym.verity.actor.typed

/**
 * Marker interface for messages, events and snapshots that are serialized with Jackson.
 */
trait Encodable
